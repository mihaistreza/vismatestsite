// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

var HtmlReporter = require('protractor-beautiful-reporter');
var today = new Date(Date.now()).toLocaleString();

exports.config = {
  allScriptsTimeout: 11000,

  suites: {
    login: './src/tests/login.e2e-spec.ts',
    listuser: './src/tests/listuser.e2e-spec.ts',
    adduser: './src/tests/adduser.e2e-spec.ts',
    edituser: './src/tests/edituser.e2e-spec.ts'
  },
  
  capabilities:
    { 'browserName': 'firefox',
    'moz:firefoxOptions': {
      binary: 'c:\\Program Files\\Mozilla Firefox\\firefox.exe'
    }},


  maxSessions: 1,
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine2',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

    onPrepare() {
  require('ts-node').register({
    project: require('path').join(__dirname, './tsconfig.e2e.json')
  });
  //Setup Reports folder
    jasmine.getEnv().addReporter(new HtmlReporter({
    baseDirectory: 'Reports/Firefox/',
    preserveDirectory: false,
    docTitle: 'Firefox_Regression_Suite ' + today,
    takeScreenShotsOnlyForFailedSpecs: true,
    screenshotsSubfolder: 'Screenshots'
 }).getJasmine2Reporter());
}
};