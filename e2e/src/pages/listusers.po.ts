import { browser, by, element, ProtractorBy } from 'protractor';
import { AddUserPage } from './adduser.po';

export class ListUsersPage {

    private btnAddUser = element(by.css('app-list-user > .btn'));
    private btnDeleteUser = element(by.xpath('//button[contains(.,\'Delete\')]'));
    private btnEditUser = element(by.xpath('//button[contains(.,\'Edit\')]'));   
    private btnConfirmDelete = element(by.css('.confirm-btn-container > .btn-danger'));
    private btnCancelDelete = element(by.css('.btn-default'));

    thID = element(by.xpath('//th[contains(.,\'Id\')]'));
    thFName = element(by.xpath('//th[contains(.,\'FirstName\')]'));
    thLName = element(by.xpath('//th[contains(.,\'LastName\')]'));
    thEmail = element(by.xpath('//th[contains(.,\'Email\')]'));
    thAction = element(by.xpath('//th[contains(.,\'Action\')]'));

    //Locator to be set in the Delete User usecase, to have it in one place, in case it should change
    by = new ProtractorBy() ;
    deletebuttonLocator = by.xpath('//button[contains(.,\'Delete\')]');

    //Elements to identify the last entry added to the bottom row
    LastRowFirstName = element.all(by.css('tr > td:nth-child(2)')).last(); 
    LastRowLastName = element.all(by.css('tr > td:nth-child(3)')).last(); 
    LastRowEmail = element.all(by.css('tr > td:nth-child(4)')).last(); 

      //Elements to identify the first entry updated to the top row
      FirstRowFirstName = element.all(by.css('tr > td:nth-child(2)')).first(); 
      FirstRowLastName = element.all(by.css('tr > td:nth-child(3)')).first(); 
      FirstRowEmail = element.all(by.css('tr > td:nth-child(4)')).first(); 

    clickAddUser(){
        this.btnAddUser.click();
        return new AddUserPage();
    }   

    clickDeleteUser(){
        this.btnDeleteUser.click();
    }   

    clickEditUser(){
        this.btnEditUser.click();
    }   

    clickCancelDelete(){
        this.btnDeleteUser.click();
        this.btnCancelDelete.click();
    }  

    clickConfirmDelete(){
        this.btnDeleteUser.click();
        this.btnConfirmDelete.click();
    }   

    //method to delete the last user after succesful add user, to bring the system to the initial state
    deleteLastUser(){
        element.all(this.deletebuttonLocator).last().click();
        this.btnConfirmDelete.click();
    }
}