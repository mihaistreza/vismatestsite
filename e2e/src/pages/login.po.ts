import { browser, by, element } from 'protractor';
import { ListUsersPage } from './listusers.po';

export class LoginPage {
    goToBasePage() {
        return browser.get(browser.baseUrl);
    }

    private emailAddress = element(by.id('email'));
    private password = element(by.id('pwd'));
    private btnLogin = element(by.buttonText('Login'));
    private form = element(by.xpath('/html/body/app-root/div/app-login/div/div/form'));

    invalidEmailErrMsg = element(by.css('.form-group:nth-child(1) > .error > div'));
    invalidPwdErrMsg = element(by.css('.form-group:nth-child(2) > .error > div'));
    invalidCredentialsMSg = element(by.css('.error > div'));

    getEmailAddressField(){
        return this.emailAddress;
    }

    getPasswordField(){
        return this.password;
    }

    getForm(){
        return this.form;
    }

    clickLoginBtn(){
        this.btnLogin.click();
    }

    login(email: string, password: string) : ListUsersPage {
        this.emailAddress.sendKeys(email);
        this.password.sendKeys(password);
        this.btnLogin.click();

        return new ListUsersPage();
    }     
}