import { element, by } from "protractor";
import { ListUsersPage } from "./listusers.po";

export class AddUserPage {

    private emailAddress = element(by.id('email'));
    private fName = element(by.id('firstName'));
    private lName = element(by.id('lastName'));
    private btnUpdate = element(by.css('.btn'));

    invalidEmailErrMsg = element(by.css('.form-group:nth-child(1) > .error > div'));
    invalidFirstNameErrMsg = element(by.css('.form-group:nth-child(2) > .error > div'));
    invalidLastNameErrMsg = element(by.css('.form-group:nth-child(3) > .error > div'));
    invalidCredentialsMsg = element(by.css('.error > div'));


    getEmailAddressField(){
        return this.emailAddress;
    }

    getFirstNameField(){
        return this.fName;
    }

    getLastNameField(){
        return this.lName;
    }

    clickUpdateBtn(){
        this.btnUpdate.click();
    }

    add(emailAddress, fName, lName){
        this.getEmailAddressField().sendKeys(emailAddress);
        this.getFirstNameField().sendKeys(fName);
        this.getLastNameField().sendKeys(lName);
        this.clickUpdateBtn();

        return new ListUsersPage();
    }
}