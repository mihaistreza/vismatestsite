
module.exports = 
{
  ValidLoginDetails:
{
    "Verify succesful login": {
      "Description": "Successful login",
      "email": "test@gmail.com",
      "password": "password",
      "expectedLoginform": "ng-valid",
    }
},
  InvalidLoginDetails:
{
    "Incorrect email/password":
    {
      "Description": "Login with incorrect email/password",
      "email": "test2@gmail.com",
      "password": "password1",
      "expected": "ng-invalid"
    },
    "Incorrect format for email address":
    {
      "Description": "Login with incorrect format for email address",
      "email": "test1",
      "password": "password",
      "expected": "ng-invalid"
    },
    "Alphanumeric/special characters email/password":
    {
      "Description": "Login with alphanumeric/specialcharacters format for email/password",
      "email": "test1!&%$#@gmail@#.com",
      "password": "45md!@$@!!",
      "expected": "ng-invalid"
    },
    "SQL injection attempt":
    {
      "Description": "SQL injection for email field",
      "email": "105'; DROP TABLE users;--",
      "password": "password",
      "expected": "ng-invalid"
    }
}   
}