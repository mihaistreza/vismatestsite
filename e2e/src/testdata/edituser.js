
module.exports = 
{
  ValidUpdateUsersDetails:
{
      "Verify succesful update": {
      "Description": "Succesful Update User",
      "email": "test1@gmail.com",
      "firstname": "John",
      "lastname": "Doe",
    }
  },
   InvalidUpdateUsersDetails:
   {
      "Incorrect format": {
      "Description": "Edit user using details in incorrect format",
      "email": "ssbff",
      "firstname": "!@$@#$",
      "lastname": "123@#$@$",
    }
  }
}