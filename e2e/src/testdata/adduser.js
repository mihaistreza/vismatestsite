
module.exports = 
{
  ValidUpdateUsersDetails:
{
      "Verify succesful update": {
      "Description": "Succesful Add User",
      "email": "test1@gmail.com",
      "firstname": "John",
      "lastname": "Doe",
    }
  },
   InvalidUpdateUsersDetails:
   {
      "Incorrect format": {
      "Description": "Add user using details in incorrect format",
      "email": "ssbff",
      "firstname": "!@$@#$",
      "lastname": "123@#$@$",
    }
  }
}