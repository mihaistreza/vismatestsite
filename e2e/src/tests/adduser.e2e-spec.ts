var data = require('../testdata/adduser.js');
var using = require('jasmine-data-provider');

import { LoginPage }from '../pages/login.po';
import { browser } from 'protractor';
import { AddUserPage } from '../pages/adduser.po';
import { ListUsersPage } from '../pages/listusers.po';

describe('Add User Testcases', () => {
    //instantiate the classes needed
    var loginPage = new LoginPage();
    var addUserPage = new AddUserPage();
    var listUsersPage = new ListUsersPage();

    beforeEach(() => {
       loginPage.goToBasePage();
       //Reach on the Add User page
       loginPage.login('test@gmail.com','password').clickAddUser();
    })

    //Method to test the succesful Add User flow
    using(data.ValidUpdateUsersDetails, (data ,description) => {
        it(data.Description, () => {

            addUserPage.add(data.email, data.firstname, data.lastname);

            //Verify the last user in the list is correctly added and matches the test data
            expect(listUsersPage.LastRowFirstName.getText()).toEqual(data.firstname);
            expect(listUsersPage.LastRowLastName.getText()).toEqual(data.lastname);
            expect(listUsersPage.LastRowEmail.getText()).toEqual(data.email);

            //Now delete the last entry in the table, to restore the system to its initial state
            listUsersPage.deleteLastUser();
        })
    })

    //Method to test the failed add user flow, using empty details
    it('Add user using empty details', () => {
        //Add user with empty details
        addUserPage.add('','','');

        //Verify error messages
        expect(addUserPage.invalidEmailErrMsg.getText()).toEqual('Email is required');
        expect(addUserPage.invalidFirstNameErrMsg.getText()).toEqual('First Name is required');
        expect(addUserPage.invalidLastNameErrMsg.getText()).toEqual('Last Name is required');
    })

     //Method to test the failed login flow, based on the login details provide in the external data file
     using(data.InvalidUpdateUsersDetails, (data ,description) => {
        it(data.Description, () => {
            addUserPage.add(data.email, data.firstname, data.lastname);

            //Expected to fail due to incorrect email format, but instead succesfully adds user
            expect(addUserPage.invalidCredentialsMsg.getText()).toEqual('Invalid credentials.');
            })
    })
})