var logindata = require('../testdata/login.js');
var using = require('jasmine-data-provider');

import { LoginPage }from '../pages/login.po';
import { browser } from 'protractor';

describe('Login Testcases', () => {
    var loginPage = new LoginPage();

    beforeEach(() => {       
       loginPage.goToBasePage();
    })

    //Method to test the succesful login flow
    using(logindata.ValidLoginDetails, (data ,description) => {
        it(data.Description, () => {
            loginPage.getEmailAddressField().sendKeys(data.email);
            loginPage.getPasswordField().sendKeys(data.password);
            expect(loginPage.getForm().getAttribute('class')).toContain(data.expectedLoginform);
            loginPage.clickLoginBtn();
            expect(browser.getCurrentUrl()).toContain('list-user');
        })
    })

    //Method to test the login flow using empty details
        it('Login using empty details', () => {
                loginPage.login('','');
                expect(loginPage.invalidEmailErrMsg.getText()).toEqual('Email is required');
                expect(loginPage.invalidPwdErrMsg.getText()).toEqual('Password is required');
            })

    //Method to test the failed login flow, based on the login details provide in the external data file
    using(logindata.InvalidLoginDetails, (data ,description) => {
        it(data.Description, () => {
            loginPage.login(data.email, data.password);           
            expect(loginPage.invalidCredentialsMSg.getText()).toEqual('Invalid credentials.');

        })
    })
})