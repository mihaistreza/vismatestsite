var data = require('../testdata/edituser.js');
var using = require('jasmine-data-provider');


import { LoginPage }from '../pages/login.po';
import { ListUsersPage } from '../pages/listusers.po';
import { EditUserPage } from '../pages/edituser.po';
import { browser, Key } from 'protractor';

describe('Edit User Testcases', () => {
    //instantiate the classes needed
    var loginPage = new LoginPage();
    var editUserPage = new EditUserPage();
    var listUsersPage = new ListUsersPage();

    beforeEach(() => {
       loginPage.goToBasePage();
       //Reach on the Edit User page and clicks the first user
       loginPage.login('test@gmail.com','password').clickEditUser();
    })

    //Method to test the succesful Edit User flow
    using(data.ValidUpdateUsersDetails, (data ,description) => {
        it(data.Description, () => {
            editUserPage.update(data.email, data.firstname, data.lastname);

            //Verify the first user updated in the list is correctly updated and matches the test data
            expect(listUsersPage.FirstRowFirstName.getText()).toEqual(data.firstname);
            expect(listUsersPage.FirstRowLastName.getText()).toEqual(data.lastname);
            expect(listUsersPage.FirstRowEmail.getText()).toEqual(data.email);
        })
    })

    //Method to test the Edit User flow with empty details
        it('Edit user using empty details', () => {
        //Workaround to clear the fields for FF
            editUserPage.getEmailAddressField().sendKeys(Key.CONTROL,"a",Key.DELETE);
            editUserPage.getEmailAddressField().sendKeys(Key.CONTROL,"a",Key.DELETE);
            editUserPage.getEmailAddressField().sendKeys(Key.CONTROL,"a",Key.DELETE);
            editUserPage.getFirstNameField().sendKeys(Key.CONTROL,"a",Key.DELETE);
            editUserPage.getLastNameField().sendKeys(Key.CONTROL,"a",Key.DELETE);
            browser.driver.sleep(2000);
            editUserPage.clickUpdateBtn();
            browser.driver.sleep(2000);

        //Verify the correct error messages are shown
            expect(editUserPage.invalidEmailErrMsg.getText()).toEqual('Email is required');
            expect(editUserPage.invalidFirstNameErrMsg.getText()).toEqual('First Name is required');
            expect(editUserPage.invalidLastNameErrMsg.getText()).toEqual('Last Name is required');
    })

   //Method to test the failed update flow, based on the test details provide in the external data file
    using(data.InvalidUpdateUsersDetails, (data ,description) => {
        it(data.Description, () => {
            editUserPage.update(data.email, data.firstname, data.lastname);
           
            //Expected to fail due to incorrect email format, but instead succesfully updates user
            expect(editUserPage.invalidCredentialsMSg.getText()).toEqual('Invalid credentials.');

        })
    })
})