var logindata = require('../testdata/login.js');
var using = require('jasmine-data-provider');

import { LoginPage } from '../pages/login.po';
import { ListUsersPage} from '../pages/listusers.po';
import { AddUserPage} from '../pages/adduser.po';
import { browser, element, by } from 'protractor';

describe('List Users Testcases', () => {
    //instantiate the classes needed
    var loginPage = new LoginPage();
    var listUsersPage = new ListUsersPage();
    var addUserPage = new AddUserPage();

    beforeAll(() => {
       loginPage.goToBasePage();
       loginPage.login('test@gmail.com', 'password');
    })

    //Method to verify all headers are present on page after succesful login
    it('Verify all table headers are present on page', () => {

        expect(listUsersPage.thID.isPresent()).toMatch('true');
        expect(listUsersPage.thFName.isPresent()).toMatch('true');
        expect(listUsersPage.thLName.isPresent()).toMatch('true');
        expect(listUsersPage.thEmail.isPresent()).toMatch('true');
        expect(listUsersPage.thAction.isPresent()).toMatch('true');
    })

    //Method to verify the Cancel button does not delete a user by comparing number of users before and after
    it('Verify Cancel-Delete button functionality', () => {

        element.all(listUsersPage.deletebuttonLocator).count().then(function (initial) {
          listUsersPage.clickCancelDelete();
            element.all(listUsersPage.deletebuttonLocator).count().then(function (after) {
              expect(initial).toEqual(after);
            })
          })
    })

    //Method to verify the Confirm button does delete a user by comparing number of users before and after
    it('Verify Confirm-Delete button functionality', () => {
        //Save the details of the first user, to use it for adding back
        var deletedEmail = listUsersPage.FirstRowEmail.getText();
        var deletedFirstName = listUsersPage.FirstRowFirstName.getText();
        var deletedLastName = listUsersPage.FirstRowLastName.getText();

        element.all(listUsersPage.deletebuttonLocator).count().then(function (initial) {
          listUsersPage.clickConfirmDelete();
            element.all(listUsersPage.deletebuttonLocator).count().then(function (after) {
              expect(initial).toEqual(after+1);
            })          
          })

          //Adding the deleted user, to bring the system to the initial state
          listUsersPage.clickAddUser();
          addUserPage.add(deletedEmail, deletedFirstName, deletedLastName);      
    })

    //Method to delete all users in the list and verify the headers are still present
    it('Delete all users', () => {
      element.all(listUsersPage.deletebuttonLocator).count().then(function (users) {
        for (let i = 0; i < users; i++) {
          listUsersPage.clickConfirmDelete();
        }
        expect(listUsersPage.thID.isPresent()).toMatch('true');
        expect(listUsersPage.thFName.isPresent()).toMatch('true');
        expect(listUsersPage.thLName.isPresent()).toMatch('true');
        expect(listUsersPage.thEmail.isPresent()).toMatch('true');
        expect(listUsersPage.thAction.isPresent()).toMatch('true');
      });

    })
})
    

