var app = angular.module('reportingApp', []);

//<editor-fold desc="global helpers">

var isValueAnArray = function (val) {
    return Array.isArray(val);
};

var getSpec = function (str) {
    var describes = str.split('|');
    return describes[describes.length - 1];
};
var checkIfShouldDisplaySpecName = function (prevItem, item) {
    if (!prevItem) {
        item.displaySpecName = true;
    } else if (getSpec(item.description) !== getSpec(prevItem.description)) {
        item.displaySpecName = true;
    }
};

var getParent = function (str) {
    var arr = str.split('|');
    str = "";
    for (var i = arr.length - 2; i > 0; i--) {
        str += arr[i] + " > ";
    }
    return str.slice(0, -3);
};

var getShortDescription = function (str) {
    return str.split('|')[0];
};

var countLogMessages = function (item) {
    if ((!item.logWarnings || !item.logErrors) && item.browserLogs && item.browserLogs.length > 0) {
        item.logWarnings = 0;
        item.logErrors = 0;
        for (var logNumber = 0; logNumber < item.browserLogs.length; logNumber++) {
            var logEntry = item.browserLogs[logNumber];
            if (logEntry.level === 'SEVERE') {
                item.logErrors++;
            }
            if (logEntry.level === 'WARNING') {
                item.logWarnings++;
            }
        }
    }
};

var defaultSortFunction = function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) {
        return -1;
    }
    else if (a.sessionId > b.sessionId) {
        return 1;
    }

    if (a.timestamp < b.timestamp) {
        return -1;
    }
    else if (a.timestamp > b.timestamp) {
        return 1;
    }

    return 0;
};


//</editor-fold>

app.controller('ScreenshotReportController', function ($scope, $http) {
    var that = this;
    var clientDefaults = {};

    $scope.searchSettings = Object.assign({
        description: '',
        allselected: true,
        passed: true,
        failed: true,
        pending: true,
        withLog: true
    }, clientDefaults.searchSettings || {}); // enable customisation of search settings on first page hit

    this.warningTime = 1400;
    this.dangerTime = 1900;

    var initialColumnSettings = clientDefaults.columnSettings; // enable customisation of visible columns on first page hit
    if (initialColumnSettings) {
        if (initialColumnSettings.displayTime !== undefined) {
            // initial settings have be inverted because the html bindings are inverted (e.g. !ctrl.displayTime)
            this.displayTime = !initialColumnSettings.displayTime;
        }
        if (initialColumnSettings.displayBrowser !== undefined) {
            this.displayBrowser = !initialColumnSettings.displayBrowser; // same as above
        }
        if (initialColumnSettings.displaySessionId !== undefined) {
            this.displaySessionId = !initialColumnSettings.displaySessionId; // same as above
        }
        if (initialColumnSettings.displayOS !== undefined) {
            this.displayOS = !initialColumnSettings.displayOS; // same as above
        }
        if (initialColumnSettings.inlineScreenshots !== undefined) {
            this.inlineScreenshots = initialColumnSettings.inlineScreenshots; // this setting does not have to be inverted
        } else {
            this.inlineScreenshots = false;
        }
        if (initialColumnSettings.warningTime) {
            this.warningTime = initialColumnSettings.warningTime;
        }
        if (initialColumnSettings.dangerTime){
            this.dangerTime = initialColumnSettings.dangerTime;
        }
    }

    this.showSmartStackTraceHighlight = true;

    this.chooseAllTypes = function () {
        var value = true;
        $scope.searchSettings.allselected = !$scope.searchSettings.allselected;
        if (!$scope.searchSettings.allselected) {
            value = false;
        }

        $scope.searchSettings.passed = value;
        $scope.searchSettings.failed = value;
        $scope.searchSettings.pending = value;
        $scope.searchSettings.withLog = value;
    };

    this.isValueAnArray = function (val) {
        return isValueAnArray(val);
    };

    this.getParent = function (str) {
        return getParent(str);
    };

    this.getSpec = function (str) {
        return getSpec(str);
    };

    this.getShortDescription = function (str) {
        return getShortDescription(str);
    };

    this.convertTimestamp = function (timestamp) {
        var d = new Date(timestamp),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh === 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    };


    this.round = function (number, roundVal) {
        return (parseFloat(number) / 1000).toFixed(roundVal);
    };


    this.passCount = function () {
        var passCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.passed) {
                passCount++;
            }
        }
        return passCount;
    };


    this.pendingCount = function () {
        var pendingCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.pending) {
                pendingCount++;
            }
        }
        return pendingCount;
    };


    this.failCount = function () {
        var failCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (!result.passed && !result.pending) {
                failCount++;
            }
        }
        return failCount;
    };

    this.passPerc = function () {
        return (this.passCount() / this.totalCount()) * 100;
    };
    this.pendingPerc = function () {
        return (this.pendingCount() / this.totalCount()) * 100;
    };
    this.failPerc = function () {
        return (this.failCount() / this.totalCount()) * 100;
    };
    this.totalCount = function () {
        return this.passCount() + this.failCount() + this.pendingCount();
    };

    this.applySmartHighlight = function (line) {
        if (this.showSmartStackTraceHighlight) {
            if (line.indexOf('node_modules') > -1) {
                return 'greyout';
            }
            if (line.indexOf('  at ') === -1) {
                return '';
            }

            return 'highlight';
        }
        return true;
    };

    var results = [
    {
        "description": "Successful login|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/styles.js 4805 WebSocket connection to 'ws://localhost:4200/sockjs-node/350/nw15a3gq/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295286139,
                "type": ""
            }
        ],
        "timestamp": 1565295284216,
        "duration": 3088
    },
    {
        "description": "Login using empty details|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/styles.js 4805 WebSocket connection to 'ws://localhost:4200/sockjs-node/350/nw15a3gq/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295287774,
                "type": ""
            }
        ],
        "timestamp": 1565295287542,
        "duration": 1703
    },
    {
        "description": "Login with incorrect email/password|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295289257,
        "duration": 1270
    },
    {
        "description": "Login with incorrect format for email address|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295290539,
        "duration": 1508
    },
    {
        "description": "Login with alphanumeric/specialcharacters format for email/password|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295292073,
        "duration": 1478
    },
    {
        "description": "SQL injection for email field|Login Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295293567,
        "duration": 1424
    },
    {
        "description": "Verify all table headers are present on page|List Users Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295296300,
        "duration": 677
    },
    {
        "description": "Verify Cancel-Delete button functionality|List Users Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295296994,
        "duration": 288
    },
    {
        "description": "Verify Confirm-Delete button functionality|List Users Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295297296,
        "duration": 849
    },
    {
        "description": "Delete all users|List Users Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "timestamp": 1565295298163,
        "duration": 1343
    },
    {
        "description": "Succesful Add User|Add User Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/polyfills.js 7125 WebSocket connection to 'ws://localhost:4200/sockjs-node/668/tvm3t2un/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295300892,
                "type": ""
            }
        ],
        "timestamp": 1565295299517,
        "duration": 3768
    },
    {
        "description": "Add user using empty details|Add User Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/polyfills.js 7125 WebSocket connection to 'ws://localhost:4200/sockjs-node/668/tvm3t2un/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295303794,
                "type": ""
            }
        ],
        "timestamp": 1565295303299,
        "duration": 2801
    },
    {
        "description": "Add user using details in incorrect format|Add User Testcases",
        "passed": false,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": [
            "Failed: No element found using locator: By(css selector, .error > div)"
        ],
        "trace": [
            "NoSuchElementError: No element found using locator: By(css selector, .error > div)\n    at elementArrayFinder.getWebElements.then (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:814:27)\n    at ManagedPromise.invokeCallback_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1376:14)\n    at TaskQueue.execute_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\n    at asyncRun (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2927:27)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:668:7\n    at process._tickCallback (internal/process/next_tick.js:68:7)Error\n    at ElementArrayFinder.applyAction_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:459:27)\n    at ElementArrayFinder.(anonymous function).args [as getText] (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:91:29)\n    at ElementFinder.(anonymous function).args [as getText] (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:831:22)\n    at UserContext.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\adduser.e2e-spec.ts:54:54)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:112:25\n    at new ManagedPromise (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1077:7)\n    at ControlFlow.promise (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2505:12)\n    at schedulerExecute (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:95:18)\n    at TaskQueue.execute_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\nFrom: Task: Run it(\"Add user using details in incorrect format\") in control flow\n    at UserContext.<anonymous> (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:94:19)\n    at attempt (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4297:26)\n    at QueueRunner.run (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4217:20)\n    at runNext (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4257:20)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4264:13\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4172:9\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:64:48\n    at ControlFlow.emit (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\events.js:62:21)\n    at ControlFlow.shutdown_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2674:10)\n    at shutdownTask_.MicroTask (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2599:53)\nFrom asynchronous test: \nError\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\adduser.e2e-spec.ts:50:9\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\node_modules\\jasmine-data-provider\\src\\index.js:37:22\n    at Array.forEach (<anonymous>)\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\node_modules\\jasmine-data-provider\\src\\index.js:30:24\n    at Suite.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\adduser.e2e-spec.ts:49:6)\n    at addSpecsToSuite (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1107:25)\n    at Env.describe (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1074:7)\n    at describe (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4399:18)\n    at Object.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\adduser.e2e-spec.ts:9:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "Screenshots\\00cf0077-0078-008f-00c3-0003005300e9.png",
        "timestamp": 1565295306154,
        "duration": 3741
    },
    {
        "description": "Succesful Update User|Edit User Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/polyfills.js 7125 WebSocket connection to 'ws://localhost:4200/sockjs-node/628/vip11dq3/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295312144,
                "type": ""
            },
            {
                "level": "WARNING",
                "message": "http://localhost:4200/styles.js 4805 WebSocket connection to 'ws://localhost:4200/sockjs-node/162/vedyvin3/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295312147,
                "type": ""
            }
        ],
        "timestamp": 1565295310337,
        "duration": 4476
    },
    {
        "description": "Edit user using empty details|Edit User Testcases",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://localhost:4200/polyfills.js 7125 WebSocket connection to 'ws://localhost:4200/sockjs-node/628/vip11dq3/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295315381,
                "type": ""
            },
            {
                "level": "WARNING",
                "message": "http://localhost:4200/styles.js 4805 WebSocket connection to 'ws://localhost:4200/sockjs-node/162/vedyvin3/websocket' failed: WebSocket is closed before the connection is established.",
                "timestamp": 1565295315381,
                "type": ""
            }
        ],
        "timestamp": 1565295314831,
        "duration": 7906
    },
    {
        "description": "Edit user using details in incorrect format|Edit User Testcases",
        "passed": false,
        "pending": false,
        "os": "Windows NT",
        "instanceId": 5764,
        "browser": {
            "name": "chrome",
            "version": "76.0.3809.100"
        },
        "message": [
            "Failed: No element found using locator: By(css selector, .error > div)"
        ],
        "trace": [
            "NoSuchElementError: No element found using locator: By(css selector, .error > div)\n    at elementArrayFinder.getWebElements.then (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:814:27)\n    at ManagedPromise.invokeCallback_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1376:14)\n    at TaskQueue.execute_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\n    at asyncRun (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2927:27)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:668:7\n    at process._tickCallback (internal/process/next_tick.js:68:7)Error\n    at ElementArrayFinder.applyAction_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:459:27)\n    at ElementArrayFinder.(anonymous function).args [as getText] (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:91:29)\n    at ElementFinder.(anonymous function).args [as getText] (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\element.js:831:22)\n    at UserContext.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\edituser.e2e-spec.ts:58:55)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:112:25\n    at new ManagedPromise (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1077:7)\n    at ControlFlow.promise (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2505:12)\n    at schedulerExecute (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:95:18)\n    at TaskQueue.execute_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\nFrom: Task: Run it(\"Edit user using details in incorrect format\") in control flow\n    at UserContext.<anonymous> (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:94:19)\n    at attempt (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4297:26)\n    at QueueRunner.run (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4217:20)\n    at runNext (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4257:20)\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4264:13\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4172:9\n    at c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:64:48\n    at ControlFlow.emit (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\events.js:62:21)\n    at ControlFlow.shutdown_ (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2674:10)\n    at shutdownTask_.MicroTask (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2599:53)\nFrom asynchronous test: \nError\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\edituser.e2e-spec.ts:54:9\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\node_modules\\jasmine-data-provider\\src\\index.js:37:22\n    at Array.forEach (<anonymous>)\n    at D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\node_modules\\jasmine-data-provider\\src\\index.js:30:24\n    at Suite.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\edituser.e2e-spec.ts:53:5)\n    at addSpecsToSuite (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1107:25)\n    at Env.describe (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1074:7)\n    at describe (c:\\Users\\mihai.streza\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4399:18)\n    at Object.<anonymous> (D:\\Tools\\protactor\\TestProject\\workspace_AngularE2E\\e2e\\src\\tests\\edituser.e2e-spec.ts:10:1)"
        ],
        "browserLogs": [],
        "screenShotFile": "Screenshots\\007a00d4-00bd-0075-004c-00d200a7004f.png",
        "timestamp": 1565295322764,
        "duration": 3905
    }
];

    this.sortSpecs = function () {
        this.results = results.sort(function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) return -1;else if (a.sessionId > b.sessionId) return 1;

    if (a.timestamp < b.timestamp) return -1;else if (a.timestamp > b.timestamp) return 1;

    return 0;
});
    };

    this.loadResultsViaAjax = function () {

        $http({
            url: './combined.json',
            method: 'GET'
        }).then(function (response) {
                var data = null;
                if (response && response.data) {
                    if (typeof response.data === 'object') {
                        data = response.data;
                    } else if (response.data[0] === '"') { //detect super escaped file (from circular json)
                        data = CircularJSON.parse(response.data); //the file is escaped in a weird way (with circular json)
                    }
                    else {
                        data = JSON.parse(response.data);
                    }
                }
                if (data) {
                    results = data;
                    that.sortSpecs();
                }
            },
            function (error) {
                console.error(error);
            });
    };


    if (clientDefaults.useAjax) {
        this.loadResultsViaAjax();
    } else {
        this.sortSpecs();
    }


});

app.filter('bySearchSettings', function () {
    return function (items, searchSettings) {
        var filtered = [];
        if (!items) {
            return filtered; // to avoid crashing in where results might be empty
        }
        var prevItem = null;

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.displaySpecName = false;

            var isHit = false; //is set to true if any of the search criteria matched
            countLogMessages(item); // modifies item contents

            var hasLog = searchSettings.withLog && item.browserLogs && item.browserLogs.length > 0;
            if (searchSettings.description === '' ||
                (item.description && item.description.toLowerCase().indexOf(searchSettings.description.toLowerCase()) > -1)) {

                if (searchSettings.passed && item.passed || hasLog) {
                    isHit = true;
                } else if (searchSettings.failed && !item.passed && !item.pending || hasLog) {
                    isHit = true;
                } else if (searchSettings.pending && item.pending || hasLog) {
                    isHit = true;
                }
            }
            if (isHit) {
                checkIfShouldDisplaySpecName(prevItem, item);

                filtered.push(item);
                prevItem = item;
            }
        }

        return filtered;
    };
});

