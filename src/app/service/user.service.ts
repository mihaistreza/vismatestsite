import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user.model';
import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}
  baseUrl = 'http://localhost:8080/user-portal/users';

  getUsersDatabase(): User[] {
    const fakeUsers: User[] = [
      { id: 1, firstName: 'Dhiraj', lastName: 'Ray', email: 'dhiraj@gmail.com' },
      { id: 2, firstName: 'Tom', lastName: 'Jac', email: 'Tom@gmail.com' },
      { id: 3, firstName: 'Hary', lastName: 'Pan', email: 'hary@gmail.com' },
      { id: 4, firstName: 'praks', lastName: 'pb', email: 'praks@gmail.com' },
    ];

    let usersDb = JSON.parse(localStorage.getItem('usersDb'));
    if (!usersDb) {
      localStorage.setItem('usersDb', JSON.stringify(fakeUsers));
      usersDb = JSON.parse(localStorage.getItem('usersDb'));
    }

    return usersDb;
  }

  getUsers() {
    return of(this.getUsersDatabase()).pipe(delay(500));
  }

  getUserById(id: number) {
    return of(this.getUsersDatabase().find(a => a.id === id));
  }

  createUser(user: User) {
    let fakeUsers = this.getUsersDatabase();
    user.id = Math.max(...fakeUsers.map(a => a.id), 0) + 1;
    fakeUsers.push(user);
    return of(localStorage.setItem('usersDb', JSON.stringify(fakeUsers)));
  }

  updateUser(user: User) {
    let fakeUsers = this.getUsersDatabase();
    let userInList = fakeUsers.find(a => a.id === user.id);
    const index = fakeUsers.indexOf(userInList, 0);
    if (index > -1) {
      fakeUsers.splice(index, 1);
    }

    userInList = user;
    fakeUsers.push(user);
    fakeUsers.sort((a, b) => (a.id > b.id ? 1 : -1));

    return of(localStorage.setItem('usersDb', JSON.stringify(fakeUsers)));
  }

  deleteUser(id: number) {
    let fakeUsers = this.getUsersDatabase();
    let userInList = fakeUsers.find(a => a.id === id);
    const index = fakeUsers.indexOf(userInList, 0);
    if (index > -1) {
      fakeUsers.splice(index, 1);
    }

    fakeUsers.sort((a, b) => (a.id > b.id ? 1 : -1));

    return of(localStorage.setItem('usersDb', JSON.stringify(fakeUsers)));
  }
}
